import 'package:flutter/material.dart';
import 'package:surveys/login.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';

void main()  {
  runApp(MaterialApp(
    home: Login(),
    debugShowCheckedModeBanner: false,
    color: Colors.blueAccent,
  ));
}
