import 'package:flutter/material.dart';
import 'package:surveys/signup.dart';
import 'login.dart';

void onSelected(BuildContext context, int item) {
  switch (item) {
    case 0:
      break;
    case 1:
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => Login(),
          ));
  }
}

class mainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.cyanAccent,
      appBar: AppBar(
        actions: [
          Theme(
              data: Theme.of(context)
                  .copyWith(iconTheme: IconThemeData(color: Colors.red)),

              child: PopupMenuButton<int>(
                  onSelected: (item) => onSelected(context, item),
                  itemBuilder: (context) => [
                    PopupMenuItem<int>(
                      value: 0,
                      child: Text('User Name'),
                    ),
                    PopupMenuDivider(),
                    PopupMenuItem<int>(
                      value: 1,
                      child: Text('Surveys'),
                    ),
                    PopupMenuItem<int>(
                      value: 2,
                      child: Text('Offers'),
                    ),
                    PopupMenuItem<int>(
                      value: 3,
                      child: Text('Activity'),
                    ),
                    PopupMenuItem<int>(
                      value: 4,
                      child: Text('Redeem Points'),
                    ),
                    PopupMenuItem<int>(
                      value: 5,
                      child: Text('Settings'),
                    ),
                    PopupMenuItem<int>(
                      value: 6,
                      child: Text('Log Out'),
                    ),
                    PopupMenuDivider(),
                    PopupMenuItem<int>(
                      value: 7,
                      child: Text('Contact Us'),
                    ),
                    PopupMenuItem<int>(
                      value: 8,
                      child: Text('Facebook'),
                    ),
                  ]))
        ],
        centerTitle: true,
        title: Text('POINTS'),
        backgroundColor: Colors.amberAccent,
      ),
      body: Container(
          child: Padding(
            padding: EdgeInsets.all(59),
            child: Column(
              children: <Widget>[
                RaisedButton(
                  onPressed: () {},
                  child: Text(
                    'Surveys',
                    style: TextStyle(color: Colors.orange, fontSize: 20),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0)),
                  padding:
                  EdgeInsets.only(right: 160, left: 160, top: 20, bottom: 20),
                  color: Colors.grey,
                ),
                SizedBox(
                  height: 50,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    RaisedButton(
                      onPressed: () {},
                      child: Text(
                        '50min',
                        style: TextStyle(color: Colors.orange, fontSize: 20),
                      ),
                      color: Colors.blueAccent,
                      elevation: 10,
                    ),
                    RaisedButton(
                      onPressed: () {},
                      child: Text(
                        '50min',
                        style: TextStyle(color: Colors.orange, fontSize: 20),
                      ),
                      color: Colors.blueAccent,
                      elevation: 10,
                    ),
                    //SizedBox(height: 20,),
                    //Padding(padding: EdgeInsets.only(right: 40,left: 40)),
                    RaisedButton(
                      onPressed: () {},
                      child: Text(
                        '50min',
                        style: TextStyle(color: Colors.orange, fontSize: 20),
                      ),
                      color: Colors.blueAccent,
                      elevation: 10,
                    ),
                  ],
                ),
                SizedBox(
                  height: 40,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    RaisedButton(
                      onPressed: () {},
                      child: Text(
                        '50min',
                        style: TextStyle(color: Colors.orange, fontSize: 20),
                      ),
                      color: Colors.blueAccent,
                      elevation: 10,
                    ),
                    RaisedButton(
                      onPressed: () {},
                      child: Text(
                        '50min',
                        style: TextStyle(color: Colors.orange, fontSize: 20),
                      ),
                      color: Colors.blueAccent,
                      elevation: 10,
                    ),
                    //SizedBox(height: 20,),
                    //Padding(padding: EdgeInsets.only(right: 40,left: 40)),
                    RaisedButton(
                      onPressed: () {},
                      child: Text(
                        '50min',
                        style: TextStyle(color: Colors.orange, fontSize: 20),
                      ),
                      color: Colors.blueAccent,
                      elevation: 10,
                    ),
                  ],
                ),
                SizedBox(
                  height: 50,
                ),
                RaisedButton(
                  onPressed: () {},
                  child: Text(
                    'Offers',
                    style: TextStyle(color: Colors.orange, fontSize: 20),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0)),
                  //elevation: 10,
                  //shape: CircleBorder(side: ),
                  padding:
                  EdgeInsets.only(right: 160, left: 160, top: 20, bottom: 20),
                  color: Colors.grey,
                ),
                SizedBox(
                  height: 50,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    RaisedButton(
                      onPressed: () {},
                      child: Text(
                        '50min',
                        style: TextStyle(color: Colors.orange, fontSize: 20),
                      ),
                      color: Colors.blueAccent,
                      elevation: 10,
                    ),
                    RaisedButton(
                      onPressed: () {},
                      child: Text(
                        '50min',
                        style: TextStyle(color: Colors.orange, fontSize: 20),
                      ),
                      color: Colors.blueAccent,
                      elevation: 10,
                    ),
                    //SizedBox(height: 20,),
                    //Padding(padding: EdgeInsets.only(right: 40,left: 40)),
                    RaisedButton(
                      onPressed: () {},
                      child: Text(
                        '50min',
                        style: TextStyle(color: Colors.orange, fontSize: 20),
                      ),
                      color: Colors.blueAccent,
                      elevation: 10,
                    ),
                  ],
                ),
                SizedBox(
                  height: 40,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    RaisedButton(
                      onPressed: () {},
                      child: Text(
                        '50min',
                        style: TextStyle(color: Colors.orange, fontSize: 20),
                      ),
                      color: Colors.blueAccent,
                      elevation: 10,
                    ),
                    RaisedButton(
                      onPressed: () {},
                      child: Text(
                        '50min',
                        style: TextStyle(color: Colors.orange, fontSize: 20),
                      ),
                      color: Colors.blueAccent,
                      elevation: 10,
                    ),
                    //SizedBox(height: 20,),
                    //Padding(padding: EdgeInsets.only(right: 40,left: 40)),
                    RaisedButton(
                      onPressed: () {},
                      child: Text(
                        '50min',
                        style: TextStyle(color: Colors.orange, fontSize: 20),
                      ),
                      color: Colors.blueAccent,
                      elevation: 10,
                    ),
                  ],
                ),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          )),
    );
  }
}
