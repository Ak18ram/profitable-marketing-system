import 'package:flutter/material.dart';
import 'signup.dart';
import 'mainpage.dart';


class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: (Color(0x0ff2b2e4a)),
      appBar: AppBar(
        title: Text('Log In'),
        backgroundColor: (Color(0x0ff903749)),
      ),
      body: Padding(
        padding: EdgeInsets.all(59.0),
        child: Column(
          children: [
            Icon(
              Icons.perm_identity,
              size: 140.0,
              color:(Color(0x0ffe84545)),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Log In ',
              style: TextStyle(color:(Color(0x0ffe84545)), fontSize: 20),
            ),
            SizedBox(
              height: 20,
            ),
            TextField(
              decoration: InputDecoration(
                border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                labelText: 'Full Name',
                hintText: 'ex: Akram Hourieh',
                labelStyle: TextStyle(color: Colors.blueGrey),
                errorStyle: TextStyle(color: Colors.red),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            TextField(
              decoration: InputDecoration(
                border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                labelText: 'Password',
                //hintText: 'ex: ********',
                labelStyle: TextStyle(color: Colors.blueGrey),
                errorStyle: TextStyle(color: Colors.red),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            TextField(
              decoration: InputDecoration(
                border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                labelText: 'Phone Number',
                hintText: 'ex: 0912345678',
                labelStyle: TextStyle(color: Colors.blueGrey),
                errorStyle: TextStyle(color: Colors.red),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            RaisedButton(
                onPressed: () {
                  //print('btn clicked');
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => mainPage(),
                      ));
                },
                child: Text(
                  'Log In',
                  style: TextStyle(color: Colors.white, fontSize: 30),
                ),
                color:(Color(0x0ffe84545)) ,
                elevation: 10 //ضوء طالع من الزر
            ),
            SizedBox(
              height: 5,
            ),
            Divider(
              height: 7,
              color: Colors.blueAccent,
            ),
            SizedBox(
              height: 10,
            ),
            InkWell(
              child: Text('create new account',
                  style: TextStyle(color: Colors.red, fontSize: 20)),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => signUp(),
                    ));
              },
            ),
          ],
        ),
      ),
    );
  }
}
