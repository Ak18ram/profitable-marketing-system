import 'package:flutter/material.dart';


class signUp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:(Color(0x0ffffd66b)),
      appBar: AppBar(
        title: Text('Sign Up'),
        backgroundColor: (Color(0x0ff654062)),
      ),
      body: Padding(
        padding: EdgeInsets.all(59),
        child: Column(children: [
          Icon(
            Icons.perm_identity,
            size: 140.0,
            color: (Color(0x0ffff9d72)),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            'Sign Up ',
            style: TextStyle(color: (Color(0x0ffeeebdd)), fontSize: 20),
          ),
          SizedBox(
            height: 20,
          ),
          TextField(
            decoration: InputDecoration(
              border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
              labelText: 'Full Name',
              hintText: 'ex: Akram Hourieh',
              labelStyle: TextStyle(color: Colors.blueGrey),
              errorStyle: TextStyle(color: Colors.red),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          TextField(
            decoration: InputDecoration(
              border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
              labelText: 'Password',
              //hintText: 'ex: ********',
              labelStyle: TextStyle(color: Colors.blueGrey),
              errorStyle: TextStyle(color: Colors.red),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          TextField(
            decoration: InputDecoration(
              border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
              labelText: 'Confirm Password',
              //hintText: 'ex: 0912345678',
              labelStyle: TextStyle(color: Colors.blueGrey),
              errorStyle: TextStyle(color: Colors.red),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                labelText: 'Phone Number',
                //hintText: 'ex: 0912345678',
                labelStyle: TextStyle(color: Colors.blueGrey),
                errorStyle: TextStyle(color: Colors.red),
              )),
          SizedBox(
            height: 20,
          ),
          RaisedButton(
            onPressed: () {
              print('btn clicked');
            },

            child: Text(
              'Sign Up',
              style: TextStyle(color: Colors.white, fontSize: 30),
            ),
            color: (Color(0x0ffff9d72)),
            elevation: 10, //ضوء لما اعمل هوفر عليه
          ),

        ]),
      ),
    );
  }
}
